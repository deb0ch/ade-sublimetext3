## ADE Sublime Text 3

- This project generates an Sublime Text 3 (https://www.sublimetext.com/) volume for [`ade`](https://gitlab.com/ApexAI/ade-cli)

### How to use

- In the `.aderc` file add, `registry.gitlab.com/deb0ch/ade-sublimetext3:latest` to the list of `ADE_IMAGES`: e.g.

```
export ADE_IMAGES="
  registry.gitlab.com/deb0ch/ade-ubuntu:latest
  registry.gitlab.com/deb0ch/ade-sublimetext3:latest
"
```

### Getting a specific Sublime Text 3 version

- Generating a volume for a different Sublime Text 3 version is as simple as creating a git tag (e.g `3211`)
- [Create an issue](https://gitlab.com/deb0ch/ade-sublimetext3/issues/new) if the desired version is not available 
