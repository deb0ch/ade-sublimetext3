
FROM ubuntu:bionic AS build_opt


ARG SUBLIME_BUILD=3211

ENV SUBLIME_ARCHIVE="sublime_text_3_build_${SUBLIME_BUILD}_x64.tar.bz2"

RUN apt-get -y update && apt-get -y install \
    curl \
    && rm -rf /var/lib/apt/lists/*

RUN curl -LO "https://download.sublimetext.com/sublime_text_3_build_${SUBLIME_BUILD}_x64.tar.bz2" \
    && tar jxvf "$SUBLIME_ARCHIVE" \
    && mv sublime_text_3 _opt \
    && mkdir -p _opt/bin \
    && ln -s ../sublime_text _opt/bin/subl \
    && rm "$SUBLIME_ARCHIVE"

COPY env.sh _opt/.env.sh
COPY adeinit _opt/.adeinit



FROM alpine


COPY --from=build_opt _opt /opt/sublimetexte3
VOLUME /opt/sublimetexte3

CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
